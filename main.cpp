#include "gc_pointer.h"
#include "LeakTester.h"

int main()
{
    //int* a = new int(19);
    Pointer<int> p = new int(19);
    p = new int(21);
    p = new int(28);
    Pointer<int>::showlist();
    return 0;
}
